from threading import Timer
from enum import Enum 
import time
import sys
import logging
import yaml
import os
import random


from modules.SerialComms import SerialComms
from modules.SerialComms import SerialMsg
from modules.Phone import Phone
from modules.Speaker import Speaker
from modules.Phonebook import Phonebook
from modules.Utils import kill_timer


LURK_TIMEOUT = 30
BECKON_TIMEOUT = 30
MAT_TIMEOUT = 10
DEMAT_TIMEOUT = 10
LIVE_TIMEOUT = 30

logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"))
log = logging.getLogger("TARDIS")

class Mode(Enum):
    LURK = 1
    MATERIALIZE = 2
    LIVE = 3
    DEMATERIALIZE = 4
    BECKON = 5

class Tardis:
    currentMode = None
    
    lurkTimer = None
    beckonTimer = None
    liveTimer = None
    dematTimer = None
    matTimer = None

    def __init__(self):
        log.info("Initializing...")
        with open('configuration.yml') as fp:
            self.config = yaml.load(fp)

        #Serial
        self.serial = SerialComms()
        self.serial.register_callback(motion_detect_callback=self.cb_motion_detected)
        self.serial.start()
        time.sleep(3)

        #Speaker
        self.speaker = Speaker()
        if not os.environ.get("SKIP_TEST"):
            self.speaker.play_file("assets/startup.mp3")
        time.sleep(3)

        #Phone
        self.phone = Phone(self.config, self.speaker, self.serial, self.set_live) 
        #TODO: Write call manager class!!! phone shouldnt have access to speaker or serial comms!!!!11

        #Phonebook -- contains directory of all interactions
        self.phonebook = Phonebook(self.phone, self.serial, self.speaker)
        self.phonebook.print_contacts()
        self.phone.set_phonebook(self.phonebook)

        #initial mode
        self.serial.write_serial(SerialMsg.STOP.value) #clears anything running on the arduino
        self.set_mode(Mode.LURK)

        #disable motion detect for first 12 seconds after boot
        self.allowMotion = False
        self.allowMotionTimer = Timer(12, self.cb_allow_motion)
        self.allowMotionTimer.start()

        log.debug("Initialization complete")

        # self.phone.call("333")

        

    def loop(self):
        if (self.currentMode == Mode.LURK):
            # log.debug("Im lurking...")
            pass

        elif (self.currentMode == Mode.BECKON):
            if(random.random() > 0.50):
                log.debug("I'm beckoning and randomly ringing")
                self.phone.single_ring()
        
        elif (self.currentMode == Mode.MATERIALIZE):
            # log.debug("Im materializing")
            pass

        elif (self.currentMode == Mode.LIVE):
            # log.debug("Im live...")
            pass

        elif (self.currentMode == Mode.DEMATERIALIZE):
            # log.debug("Im dematerializing...")
            pass


#oh god, my eyes
    def set_mode(self, mode):
        log.info("Setting Tardis mode to " + mode.name)
        self.currentMode = mode

        if (self.currentMode == Mode.LURK):
            log.debug("Setting lurk timer")
            self.speaker.play_file("assets/tardis-lurking.wav")
            self.lurkTimer = Timer(LURK_TIMEOUT, self.cb_lurktimer_fire)
            self.lurkTimer.start()
            self.serial.write_serial(SerialMsg.STOP.value) 
            time.sleep(0.2)
            self.serial.write_serial(SerialMsg.MODE_LURK.value)

        elif (self.currentMode == Mode.BECKON):
            log.debug("Setting beckon timer")
            self.beckonTimer = Timer(BECKON_TIMEOUT, self.cb_beckontimer_fire)
            self.beckonTimer.start()
            self.serial.write_serial(SerialMsg.MODE_BECKON.value)
        

        elif (self.currentMode == Mode.MATERIALIZE):
            log.debug("Setting materialize timer")
            self.speaker.play_file("assets/tardis-mat.wav")
            self.kill_all_timers() #ensure no other timers are live that will stomp on mode
            self.matTimer = Timer(MAT_TIMEOUT, self.cb_mattimer_fire)
            self.matTimer.start()
            self.serial.write_serial(SerialMsg.MODE_MAT.value)
            time.sleep(0.2)
            self.serial.write_serial(SerialMsg.FAST_FADE_FLOODS.value)
        
        elif (self.currentMode == Mode.DEMATERIALIZE):
            log.debug("Setting dematerialize timer")
            self.speaker.play_file("assets/tardis-demat.wav")
            self.kill_all_timers() #ensure no other timers are live that will stomp on mode
            self.dematTimer = Timer(DEMAT_TIMEOUT, self.cb_demattimer_fire)
            self.dematTimer.start()
            self.serial.write_serial(SerialMsg.MODE_DEMAT.value)
            time.sleep(0.2)
            self.serial.write_serial(SerialMsg.FAST_FADE_FLOODS.value)

        elif (self.currentMode == Mode.LIVE):
            log.debug("Setting live timer")
            self.kill_all_timers()
            self.liveTimer = Timer(LIVE_TIMEOUT, self.cb_livetimer_fire)
            self.liveTimer.start()
            self.serial.write_serial(SerialMsg.STOP.value) 
            time.sleep(0.2) #kill it with fire
            self.serial.write_serial(SerialMsg.MODE_LIVE.value)


    def cb_allow_motion(self):
        log.debug("allowing motion")
        self.allowMotion = True

    def cb_motion_detected(self):
        log.info("Motion detected")
        if((self.currentMode == Mode.LURK or self.currentMode == Mode.BECKON) and self.allowMotion == True):
            self.phone.single_ring()
            self.set_mode(Mode.MATERIALIZE)
        else:
            log.debug("We're not currently lurking/beckoning ignoring motion")

    def cb_lurktimer_fire(self):
        log.debug("Lurk timer elapsed.")
        self.set_mode(Mode.BECKON)

    def cb_beckontimer_fire(self):
        log.debug("Beckon timer elapsed.")
        self.set_mode(Mode.LURK)

    def cb_mattimer_fire(self):
        log.debug("materialize timer elapsed.")
        self.set_mode(Mode.LIVE)

    def cb_livetimer_fire(self):
        log.debug("Live timer elapsed.")
        self.set_mode(Mode.LURK)

    def cb_demattimer_fire(self):
        log.debug("Dematerialize timer elapsed.")
        self.set_mode(Mode.LURK)

    def set_live(self):
        self.set_mode(Mode.LIVE)

    def kill_all_timers(self):
        log.debug("Killing all timers")
        kill_timer(self.lurkTimer)
        kill_timer(self.beckonTimer)
        kill_timer(self.dematTimer)
        kill_timer(self.matTimer)
        kill_timer(self.liveTimer)

    def stop(self):
        log.info("Safely shutting down tardis")
        self.serial.stop()
        self.kill_all_timers()
        self.phone.stop()



if __name__ == "__main__":
    try:
        tardis = Tardis()
        while True:
            tardis.loop()
            time.sleep(1)

    except KeyboardInterrupt:
        tardis.stop()
        sys.exit(0)

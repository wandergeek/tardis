from modules.Contact import Contact
from modules.SerialComms import SerialMsg
import time
import random
import os
import logging
import pyttsx

#TODO: really don't want to repeat this for every module
logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
log = logging.getLogger("RICKROLL")

class Rickroll(Contact):

    def __init__(self, phone, serial, speaker):
        self.name = "rickroll"
        self.number = "333"
        self.weight = "0.0"
            
        super().__init__(phone, serial, speaker, self.name, self.number)
    
    def run(self):
        self.phone.handset.speak("I have a confession. You're the only person I can tell. Listen very carefully. Are you ready?", sleep=1.5)
        self.phone.handset.speak("We first met on tinder, and after several dates he took me to a French restaurant with beautiful stained glass windows and amazing service. I felt so sophisticated! We enjoyed five courses and several glasses of wine. We went back to his place, where he quickly progressed from kissing me to touching me. He opened the door to his bedroom and I stripped off his clothes, at this point I could see his huge", cb=self.cb_rickrollify)
        
        


    def cb_rickrollify(self, future): 
        self.serial.write_serial(SerialMsg.MODE_666.value)
        self.speaker.play_file("assets/music/rickroll.wav")

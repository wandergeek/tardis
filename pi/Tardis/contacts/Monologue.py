from modules.Contact import Contact
import time
import random
import os
import logging

#TODO: really don't want to repeat this for every module
logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
log = logging.getLogger("MONOLOGUE")

class Monologue(Contact):

    def __init__(self, phone, serial, speaker):
        self.name = "monologue"
        self.number = "444"
        self.weight = "0.7"
        super().__init__(phone, serial, speaker, self.name, self.number)
    
    def run(self):
        self.phone.handset.speak("Hello. You are being transported back in time. Stay for a spell.", cb=self.play_monologue)
    
    def play_monologue(self, future):
        random_file=random.choice(os.listdir("assets/monologues"))
        self.speaker.play_file('assets/monologues/' + random_file)

import time
import random
import os
import logging
import pyttsx

from modules.Contact import Contact
from modules.SerialComms import SerialMsg

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
log = logging.getLogger("420")

class Fourtwenty(Contact):

    def __init__(self, phone, serial, speaker):
        self.name = "420"
        self.number = "420"
        self.weight = "0.0"
            
        super().__init__(phone, serial, speaker, self.name, self.number)

    def run(self):
        self.phone.handset.speak("Jah, rastafari. Spark a spliff and stay a while.", sleep=2)
        self.speaker.play_file("assets/music/420.mp3")
        self.serial.write_serial(SerialMsg.MODE_420.value)


        
        
        




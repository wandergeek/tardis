import time
import random
import os
import logging

from modules.SerialComms import SerialMsg
from modules.Contact import Contact

#TODO: really don't want to repeat this for every module
logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
log = logging.getLogger("GAY")

class Gay(Contact):

    def __init__(self, phone, serial, speaker):
        self.name = "gay"
        self.number = "555"
        self.weight = "0.0"
            
        super().__init__(phone, serial, speaker, self.name, self.number)
    
    def run(self):
        self.phone.handset.speak("Girl, it's time to dance.",cb=self.gayify)
        
    def gayify(self, future):
        self.serial.write_serial(SerialMsg.FAST_FADE_FLOODS.value)
        self.serial.write_serial(SerialMsg.MODE_GAY.value)
        self.speaker.play_file("assets/music/george-michael-freedom.mp3")
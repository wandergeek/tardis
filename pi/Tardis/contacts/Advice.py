import time
import random
import os
import logging
import datetime

from modules.SerialComms import SerialMsg
from modules.Contact import Contact

#TODO: really don't want to repeat this for every module
logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
log = logging.getLogger("ADVICE")

class Advice(Contact):

    filename = None

    def __init__(self, phone, serial, speaker):
        self.name = "advice"
        self.number = "888"
        self.weight = "0.3"

            
        super().__init__(phone, serial, speaker, self.name, self.number)
    
    def run(self):
        self.phone.handset.speak("Hello. I have an important question I'd like you to answer.", sleep=4)
        self.phone.handset.speak("What advice would you give your past self? I'm listening intently.", sleep=4)
        self.filename="assets/monologues/advice-" + str(int(time.time())) + ".wav"
        self.phone.handset.record(filename=self.filename,seconds=10, distort=True)
        time.sleep(0.5)
        self.phone.handset.speak("You sound pretty strange. Are you ok?.", cb=self.playback_advice)

    def playback_advice(self, future):
        self.speaker.play_file(self.filename)
        
        

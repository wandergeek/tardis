import yaml
import logging
import os
from pygame import mixer
import pyttsx
from pysndfx import AudioEffectsChain
import pyaudio
import wave
from shutil import move
import time
from subprocess import check_output, Popen
import subprocess
from concurrent.futures import ThreadPoolExecutor as Pool


logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"))
log = logging.getLogger("HANDSET")

class Handset: 

    config = None
    engine = None
    ch1 = None
    ch2 = None
    ch3 = None
    soundVolume = 1
    speakQueue = []
    pool = None
    speech_speed = "150" #words per minute

    def __init__(self, config):
        log.debug("Initializing handset")

        self.config = config
        self.onHook = True

        #sound files
        mixer.init()
        self.ch1 = mixer.Channel(1)
        self.ch2 = mixer.Channel(2)
        self.ch3 = mixer.Channel(3)

        self.pool = Pool(max_workers=1)

        

    def play_file(self,filename, wait=False):
        log.info("playing file " + filename)
        s = mixer.Sound(filename) 
        s.set_volume(self.soundVolume)
        self.ch1.play(s) #just using channel one for now
        while(wait == True and self.is_busy()):
            # log.debug(filename + " is playing, sleeping")
            time.sleep(0.2)
    
    def play_ringtone(self):
        self.play_file(self.config["soundfiles"]["dialtone"])

    def play_ring(self):
        self.play_file(self.config["soundfiles"]["ring"], wait=True)

    def ring(self, num_rings=3):
        for r in range(1,num_rings):
            log.debug("Ringing...")
            self.play_ring()


    def stop(self):
        self.ch1.stop()
        log.debug("Stopping speech")
        try:
            subprocess.Popen(["pkill", "espeak"])
            # self.pool.shutdown(wait=False) # no .submit() calls after that point
        except:
            log.warning("Couldnt stop espeak. Meh.")
            
        

    def pause(self):
        self.ch1.pause()

    def unpause(self):
        self.ch1.unpause()

    def is_busy(self):
        return mixer.get_busy()

    def set_volume(self,volume):
        log.debug("Setting volume to " + str(volume))
        self.soundVolume = volume

    def record(self, seconds=5, filename='tmp/phone-recording.wav', distort=False):
        log.debug("Recording audio...")
        rec_format = pyaudio.paInt16
        rec_channels = 1
        sample_rate=44100
        chunk = 512

        audio = pyaudio.PyAudio()

        stream = audio.open(format=rec_format,
                        channels=rec_channels,
                        rate=sample_rate,
                        input=True,
                        frames_per_buffer=chunk) #buffer

        frames = []

        for i in range(0, int(sample_rate / chunk * seconds)):
            data = stream.read(chunk)
            frames.append(data) # 2 bytes(16 bits) per channel

        stream.stop_stream()
        stream.close()
        audio.terminate()

        wf = wave.open(filename, 'wb')
        wf.setnchannels(rec_channels)
        wf.setsampwidth(audio.get_sample_size(rec_format))
        wf.setframerate(sample_rate)
        wf.writeframes(b''.join(frames))
        wf.close()

        if distort == True:
            log.debug("Distorting audio")
            self.distort_audio(filename)
	

    def distort_audio(self, filename):
        print("Distorting audio...")
        fx = (
            AudioEffectsChain()
            .highshelf()
            .reverb()
            .phaser()
            .delay()
            .lowshelf()
        )

        infile = filename
        outfile = 'tmp/processed.wav'
        fx(infile, outfile)
        move(outfile, infile)




    def wait(self):
        while(self.is_speaking()):
            time.sleep(0.2)

    def is_speaking(self):
        cmd_output = os.popen("ps aux").read()
        if "espeak" in cmd_output[:]:
            log.debug("voice is currently speaking.")
            return True
        else:
            log.debug("voice is not currently speaking.")
            return False

    def set_voice(self,voiceID):
        log.debug("Setting voice to " + voiceID)
        self.engine.setProperty('voice', voiceID)
   

    def on_hook(self):
        self.stop()
        self.onHook = True

    def off_hook(self):
        self.play_ringtone()
        self.onHook = False



    def cb_default_speak_finish(self,future):
        if future.exception() is not None:
            log.info("got exception: %s" % future.exception())
        else:
            log.info("process returned %d" % future.result())
    
    def cb_sleep(self, s):
        log.debug("sleeping for " + str(s) + " seconds...")
        time.sleep(s)
    
    def speak(self, text, cb=None, sleep=0):
        # self.engine.setProperty('rate', 150)
        log.debug("Got request to say " + text)  
        if(self.onHook == False):
            f = self.pool.submit(subprocess.call, ["/usr/bin/espeak", "-s", self.speech_speed, text], shell=False)
            if cb is not None:
                f.add_done_callback(cb)
            if sleep != 0:
                f.add_done_callback(self.cb_sleep(sleep))
            else:
                f.add_done_callback(self.cb_default_speak_finish)
            # log.info("continue waiting asynchronously")
            # p = subprocess.Popen(["/usr/bin/espeak", text])
            # p.wait()
            # self.wait()
        else:
            log.debug("..but phone is on the hook so ignoring it.")
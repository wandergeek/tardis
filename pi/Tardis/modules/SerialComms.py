import serial
import logging
import threading
from enum import Enum
import os
import time

SERIAL_DEVICE = "/dev/ttyACM0"
logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"))
log = logging.getLogger("SERIAL")

class SerialMsg(Enum):
    
    MODE_LURK="0"
    MODE_BECKON="1"
    MODE_MAT="2"
    MODE_LIVE="3"
    MODE_DEMAT="4"
    MODE_666="5"
    MODE_GAY="6"
    MODE_420="7"
    STOP="8"
    MOTION_DETECTED="10"
    FAST_FADE_FLOODS="9"
    SLOW_FADE_FLOODS="21"
    KILL_LAMPS="22"


    

class SerialComms(threading.Thread):
    ser = None
    _is_running = True

    def __init__(self):
        log.debug("Initializing serial...")
        try:
            self.ser = serial.Serial(SERIAL_DEVICE, 9600)
            time.sleep(1) #chill a sec while serial boots
        except (OSError, serial.SerialException):
            log.warn("Couldn't attach to serial " + str(SERIAL_DEVICE) + ". Continuing.")
        threading.Thread.__init__(self)
        log.debug("Serial init complete")


    def register_callback(self, motion_detect_callback):
        self.motion_detect_callback = motion_detect_callback
    
    def run(self):
        while self._is_running and self.ser is not None:
            value = self.ser.readline()
            v = value.decode("utf-8").rstrip()
            log.debug("Serial received message: " + str(v))
            
            if(v == SerialMsg.MOTION_DETECTED.value):
                log.debug("Detected motion detect msg, calling cb")
                self.motion_detect_callback()

    def write_serial(self,msg):
        if self.ser is not None:
            try:
                packet = "<" + str(msg) + ">"
                log.debug("writing serial messsage " + packet)
                self.ser.write(packet.encode())
                self.ser.flush()
            except Exception as e:
                log.warn("Tried to send following serial message but failed:  " + str(msg) + "err:" + str(e))

    def stop(self):
        self._is_running = False
        if(self.ser is not None):
            self.ser.write(SerialMsg.STOP.value)

    def test(self):
        for m in SerialMsg:
            log.info("testing: " + str(m))
            self.serial.write_serial(m.value)
            time.sleep(3)


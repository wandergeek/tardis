import yaml
import logging
import gpiozero
import os
import time

from modules.RotaryDial import RotaryDial
from modules.Handset import Handset

#for the love of god, get this out of here
from modules.SerialComms import SerialMsg

logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"))
log = logging.getLogger("PHONE")

PIN_LEFT_RING = 23
PIN_RIGHT_RING = 24
PIN_HOOKSWITCH = 8

class Phone:

    dial = None
    handset = None
    phonebook = None

    ##TODO :REMOVE THIS
    speaker = None

    def __init__(self, config, speaker, serial,set_live):
        log.debug("Initializing phone")
        #TODO: GET RID OF THIS!!
        self.speaker = speaker
        self.set_live = set_live
        self.serial = serial
        #####

        self.config = config
        self.dial = RotaryDial()
        self.dial.register_callback(cb_dial_number=self.call, cb_got_digit=self.cb_got_digit) 
        #cb_dial_number dialer calls this function when user has finished dialing
        #cb_got_digit dialer calls function when user has dialed first digit

        self.hookswitch = gpiozero.Button(pin=PIN_HOOKSWITCH, pull_up=True, bounce_time=0.05)
        self.hookswitch.when_pressed = self.cb_hookswitch_pressed
        self.hookswitch.when_released = self.cb_hookswitch_released

        self.handset = Handset(config)

        self.leftRing = gpiozero.OutputDevice(PIN_LEFT_RING)
        self.rightRing = gpiozero.OutputDevice(PIN_RIGHT_RING)

        
        if not os.environ.get("SKIP_TEST"):
            log.debug("Testing ringer...")
            time.sleep(1)
            self.single_ring()
            log.debug("Testing handset...")
            self.handset.set_volume(1)
            while(self.handset.is_busy()):
                pass
            self.handset.speak("Ready to work, captain")
            self.handset.set_volume(0.75)

    def stop(self):
        self.kill_ringer()
        self.handset.stop()
        self.dial.stop()

    def single_ring(self):
        log.info("Ringing...")
        for rings in range(0, 10):
            self.rightRing.off()
            self.leftRing.on()

            time.sleep(0.05)

            self.leftRing.off()
            self.rightRing.on()

            time.sleep(0.05)

            self.rightRing.off()
            self.leftRing.off()

    def kill_ringer(self):
        self.rightRing.off()
        self.leftRing.off()

#if number dialed exists, dial it, otherwise get a random one
    def call(self, number):
        if(self.phonebook.does_contact_exist(number)):
            contact = self.phonebook.get_contact_by_number(number)
            log.info("contact exists, dialing " + str(contact))
        else:
            contact = self.phonebook.get_random_contact()
            log.info("contact doesnt exist, dialing " + str(contact))

        log.info("calling " + str(contact))
        self.dial.cancel_dial_timer()
        self.handset.ring()
        contact.dial()
        
    #move this to call manager class
    def reset_tardis(self):
        log.debug("Resetting tardis")
        self.kill_ringer()
        self.speaker.stop()
        self.serial.write_serial(SerialMsg.KILL_LAMPS.value) #for the love of god get this shit out of here
        self.set_live() #after contact exits, set tardis back to live

    def set_phonebook(self, phonebook):
        log.info("setting phonebook")
        self.phonebook = phonebook

    def cb_hookswitch_pressed(self):
        log.debug("Phone off hook")
        self.handset.off_hook()

    def cb_hookswitch_released(self):
        log.debug("Phone on hook")
        self.handset.on_hook()
        self.reset_tardis() #move to call manager

    def cb_got_digit(self):
        log.debug("Dial has notified phone about first digit. Stopping ringtone")
        self.handset.stop()

import signal
from subprocess import check_output, Popen
import subprocess
import logging
import os
import sys

logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"))
log = logging.getLogger("SPEAKER")
SHUT_UP=os.environ.get("SHUT_UP", "FALSE")


class Speaker:
    ser = None
    _is_running = True
    pid = None
    

    def __init__(self):
        log.debug("Initializing speaker...")

    def play_file(self, filename):
        if SHUT_UP == "TRUE":
            log.debug("Skipping file play")
        else:
            log.debug("Playing file " + filename)
            with open("/tmp/playfile.log","wb") as out, open("stderr.txt","wb") as err:
                subprocess.Popen(["omxplayer", filename],stdout=out,stderr=err, shell=False)

    def stop(self):
        log.debug("Stopping speaker")
        try:
            subprocess.Popen(["pkill", "omxplayer"])
        except:
            log.warning("Couldnt stop speaker. Meh.")
            
            
import logging
import os
import time

logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"))
log = logging.getLogger("CONTACT")

class Contact:


    def __init__(self, phone, serial, speaker, name, number):
        self.phone = phone
        self.serial = serial
        self.speaker = speaker

        self.name = name
        self.number = number
        self.terminated = False

        
    def dial(self):
        log.info("Contacting " + self.name + " at " + self.number)
        self.run()

    def run(self):
        log.debug(self.name + "is running...")
        return

    def stop(self):
        log.info("Hanging up on " + self.name + " at " + self.number)
    
    def __str__(self):
        return self.name + " at " + self.number

    def sleep(self, s):
        if(self.terminated == False):
            time.sleep(s)

    def terminate(self):
        self.terminated = True
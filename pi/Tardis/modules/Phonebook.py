import yaml
import logging
import os
import pkgutil 
import importlib
from pathlib import Path
from os.path import basename
import random
#from numpy.random import choice

logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"))
log = logging.getLogger("PHONEBOOK")

class Phonebook: 

    book = []
    weights = []

    def __init__(self, phone, serial, speaker):
        log.debug("Initializing phonebook")
        pathlist = Path("contacts/").glob('*.py')
        for path in pathlist:
            module = importlib.import_module('contacts.' + path.stem)
            # log.debug("Processing " + path.stem)
            my_class = getattr(module, path.stem)
            my_instance = my_class(phone, serial, speaker)
            self.book.append(my_instance)
            self.weights.append(my_instance.weight)

    def print_contacts(self):
        log.info("Printing contacts in phonebook:")
        for c in self.book:
            log.info("      " + str(c))

    
    # def call(self, number):
    #     log.debug("Received request to call " + str(number))
    #     matched = False
    #     for c in self.book:
    #         if c.number == number:
    #             log.debug("We've got a match. Dialing.")
    #             matched = True
    #             c.dial()
    #     if(matched == False):
    #         randomContact = 
    #         log.debug("No match found. Dialing some random shit: " + str(randomContact))
    #         randomContact.dial()


    def get_random_contact(self):
        return random.choice(self.book)
        #not totally random, uses weights -- joke/monologue more likely to get chosen
#        return choice(self.book, p=self.weights)
        

    def does_contact_exist(self, number):
        match = False
        for c in self.book:
            if c.number == number:
                match = True
        return match

    def get_contact_by_number(self,number):
        for c in self.book:
            if c.number == number:
                return c
        #throw error here?

import os
import Queue
import threading
import signal
import sys
import yaml


from threading import Timer
from modules.Ringtone import Ringtone
from modules.RotaryDial import RotaryDial


callback_queue = Queue.Queue()

class TardisPhone:
    # Number to be dialed
    dial_number = ""

    # On/off hook state
    offHook = False

    offHookTimeoutTimer = None
    RotaryDial = None
    config = None

    def __init__(self):
        print "[STARTUP]"
        signal.signal(signal.SIGINT, self.OnSig)
        
        self.config = yaml.load(file("configuration.yml",'r'))
        self.Ringtone = Ringtone(self.config)
        # self.Ringtone.playfile(self.config["soundfiles"]["startup"])

        self.RotaryDial = RotaryDial()
        self.RotaryDial.RegisterCallback(NumberCallback = self.GotDigit, OffHookCallback = self.OffHook, OnHookCallback = self.OnHook, OnVerifyHook = self.OnVerifyHook)
        print "[STARTUP COMPLETE]"
        # self.Ringtone.doring()
        self.Ringtone.playfile('/home/pi/tardis/phone/TardisPhone/assets/rickroll.mp3')
        raw_input("Waiting.\n")
    

    def OnVerifyHook(self, state):
        if state:
            # print "taking phone offhook?"
            self.offHook = False
            # print "on verify stopping stuff, saying its offhook"
            self.Ringtone.stophandset()

    def GotDigit(self, digit):
        print "[DIGIT] Got digit: %s" % digit
        print "stopping handset from gotdigit"
        self.Ringtone.stophandset()
        self.dial_number += str(digit)
        print "[NUMBER] We have: %s" % self.dial_number

        

        if len(self.dial_number) == 2:
            if self.offHook:
                print "[PHONE] Dialing number: %s" % self.dial_number

                self.dial_number = ""

    def OnHook(self):
        print "[PHONE] On hook"
        self.offHook = False
        print "stopping handset from onhook"
        self.Ringtone.stophandset()

    def OffHook(self):
        print "[PHONE] Off hook"
        
        self.offHook = True
        # Reset current number when off hook
        self.dial_number = ""
        # self.offHookTimeoutTimer = Timer(5, self.OnOffHookTimeout)
        # self.offHookTimeoutTimer.start()

        # TODO: State for ringing, don't play tone if ringing :P
        self.Ringtone.starthandset(self.config["soundfiles"]["dialtone"])

        self.Ringtone.stop()

    def OnOffHookTimeout(self):
        print "[OFFHOOK TIMEOUT]"
        # self.Ringtone.stophandset()
        #self.Ringtone.starthandset(self.config["soundfiles"]["timeout"])


    def stop(self):
        self.RotaryDial.StopVerifyHook()
        self.Ringtone.kill_ringer()

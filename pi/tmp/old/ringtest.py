import gpiozero
import time

leftRing = gpiozero.OutputDevice(23)
rightRing = gpiozero.OutputDevice(24)

def single_ring():
    print("ringing...")
    for rings in range(0, 10):
        rightRing.off()
        leftRing.on()

        time.sleep(0.05)

        leftRing.off()
        rightRing.on()

        time.sleep(0.05)

        rightRing.off()
        leftRing.off()

single_ring()
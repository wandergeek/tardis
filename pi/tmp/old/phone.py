import gpiozero
import time
import sys
import subprocess
import random
import pyaudio
import wave
from pysndfx import AudioEffectsChain
import serial
import os
import signal
import subprocess
import shlex

class TardisPhone:

	voiceProc = None

	def __init__(self):
		self.leftRing = gpiozero.OutputDevice(23)
		self.rightRing = gpiozero.OutputDevice(24)

		self.hookswitch = gpiozero.Button(pin=8, pull_up=True, bounce_time=0.05)
		self.hookswitch.when_pressed = self.cb_hookswitch_pick_up
		self.hookswitch.when_released = self.cb_hookswitch_hang_up
		# self.phone_off_hook = False

		self.dial = gpiozero.Button(pin=25, pull_up=True, bounce_time=0.06)
		# self.dial.when_pressed = self.cb_dial_triggered

		self.is_ringing = False
                
		try:
			self.ser = serial.Serial('/dev/ttyACM0',9600)
		except (OSError, serial.SerialException):
			pass
		
		self.write_serial('9')

		

		

	
	def cb_hookswitch_pick_up(self):
		print "phone picked up"
		self.phone_off_hook = True
		self.kill_ringer()

	def cb_hookswitch_hang_up(self):
		print "phone hung up"
		self.phone_off_hook = False
        #reset any lighting effects, this should be an enum
		self.write_serial('9')

 		if self.voiceProc is not None and self.voiceProc.poll():
			print "voice process running, killing"
			self.voiceProc.kill()


	def cb_dial_triggered(self):
		print "dial pressed"


	def speak(self, text, wait=False):
		# subprocess.call(['/usr/bin/espeak', text])
		# p = subprocess.Popen(['/usr/bin/espeak', text],  stdout=subprocess.PIPE, shell=True)
		print "saying " + text
		self.voiceProc = subprocess.Popen(["/usr/bin/espeak", text], stdout=subprocess.PIPE, shell=False)
		if wait:
			print "waiting for speak to finish"
			self.voiceProc.wait()

	def loop(self):
		"""Main polled loop"""
		print "start loop"
		if self.hookswitch.is_pressed:
			print "loop: phone off hook"
			# Handset is raised
			if self.is_ringing:
				print "loop: its ringing"
				# User has picked up a ringing phone
				random_number = random.randint(0,9)
				print "dialing random number" + random_number
				self.call(random_number)
				self.is_ringing = False
			else:
				# print "picked up silent phone"
				# self.speak("Hello traveller. Please dial a number.", wait=True)
				# User has picked up a silent phone, wait for them to dial something
				while self.dial.is_pressed and self.hookswitch.is_pressed:
					last_dial_state = True
				
				# Wait for dialling to complete
				dialled_number = 0
				last_timestamp = time.time()
				while (time.time() - last_timestamp) < 0.3:
					current_dial_state = self.dial.is_pressed
					if not current_dial_state and last_dial_state == True:
						dialled_number = dialled_number + 1
						last_timestamp = time.time()
						print "incrementing to " + str(dialled_number)
						
					last_dial_state = current_dial_state
					
					# self.speak("Calling " + str(dialled_number), wait=True)
					# self.call(dialled_number)
				
		# else:
		# 	# Decide whether or not to ring
		# 	if self.is_ringing or random.random() > 0.95:
		# 		print "random ring triggered"
		# 		self.is_ringing = True
				
		# 		# Ring until someone answers
		# 		self.single_ring()



	def call(self, number):
		print "hi"
		# if number == 1:
		# 	self.trippy_talk()
		# else:
		# 	print str(number) + " not implemented yet"

	def trippy_talk(self):
		self.speak("Is that you? I have a question for you.", wait=True)
		time.sleep(0.5)
		self.speak("Have you ever drunk baileys from a shoe? Answer truthfully. I'm listening.", wait=True)
		self.record()
		self.distort_audio()
		self.speak("Are you ok? You sound very strange.", wait=True)
		self.write_serial('1')
		self.play()

	

	def single_ring(self):
		"""Short ringing burst"""
		print "ringing..."
		for rings in range(0, 10):
			self.rightRing.off()
			self.leftRing.on()

			time.sleep(0.05)

			self.leftRing.off()
			self.rightRing.on()

			time.sleep(0.05)

			self.rightRing.off()
			self.leftRing.off()

	def write_serial(self,msg):
		try:
			self.write_serial(msg)
		except:
			print "Serial not hooked up. No worries m8."
		
		

	def record(self, seconds=5, filename='tmp/phone-recording.wav'):
		""" Record a wave file from the handset"""
		print "recording..."
		rec_format = pyaudio.paInt16
		rec_channels = 1
		sample_rate=44100
		chunk = 512
		
		audio = pyaudio.PyAudio()

		stream = audio.open(format=rec_format,
						channels=rec_channels,
						rate=sample_rate,
						input=True,
						frames_per_buffer=chunk) #buffer

		frames = []

		for i in range(0, int(sample_rate / chunk * seconds)):
			data = stream.read(chunk)
			frames.append(data) # 2 bytes(16 bits) per channel

		stream.stop_stream()
		stream.close()
		audio.terminate()

		wf = wave.open(filename, 'wb')
		wf.setnchannels(rec_channels)
		wf.setsampwidth(audio.get_sample_size(rec_format))
		wf.setframerate(sample_rate)
		wf.writeframes(b''.join(frames))
		wf.close()
	

	def distort_audio(self):
		print "Distorting audio..."
		fx = (
			AudioEffectsChain()
			.highshelf()
			.reverb()
			.phaser()
			.delay()
			.lowshelf()
		)

		infile = 'tmp/phone-recording.wav'
		outfile = 'tmp/processed-phone-recording.wav'
		fx(infile, outfile)

	def play(self, filename='tmp/processed-phone-recording.wav'):
		""" Play back a wave file through the handset"""
		chunk = 1024

		try:
			wf = wave.open(filename, 'rb')
			audio = pyaudio.PyAudio()
			stream = audio.open(format=audio.get_format_from_width(wf.getsampwidth()),
								channels=wf.getnchannels(),
								rate=wf.getframerate(),
								output=True)
			data = wf.readframes(chunk)

			while len(data) > 0:
				stream.write(data)
				data = wf.readframes(chunk)

			stream.stop_stream()
			stream.close()
			audio.terminate()
		except FileNotFoundError:
			print('WAV file not found')
	
	
	def kill_ringer(self):
		"""Ensure the ringer is not powered when we exit"""
		self.rightRing.off()
		self.leftRing.off()

if __name__ == "__main__":
	try:
		phone = TardisPhone()
		while True:
			phone.loop()
			time.sleep(1)

	except KeyboardInterrupt:
		phone.kill_ringer()
		sys.exit(0)



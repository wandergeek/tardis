import gpiozero
import time
import sys
import subprocess
import random
import pyaudio
import wave

class TardisPhone:
	def __init__(self):
		self.leftRing = gpiozero.OutputDevice(23)
		self.rightRing = gpiozero.OutputDevice(24)
		self.hookswitch = gpiozero.Button(pin=8, pull_up=True, bounce_time=0.05)
		self.dial = gpiozero.Button(pin=25, pull_up=True, bounce_time=0.03)
		self.is_ringing = False

	def loop(self):
	# """"Main polled loop""""
		if self.hookswitch.is_pressed:
			# Handset is raised
			if self.is_ringing:
				# User has picked up a ringing phone
				subprocess.call(['/usr/bin/espeak', '"Hello? Hello? Can you hear me?"'])
				self.record()
				subprocess.call(['/usr/bin/espeak', '"lalalalalalalalalalalaaaaaaaaaaaaaaaaaaaaaaaaa"'])
				self.is_ringing = False
			else:
				# User has picked up a silent phone, wait for them to dial something
				while self.dial.is_pressed and self.hookswitch.is_pressed:
					last_dial_state = True
				
				# Wait for dialling to complete
				dialled_number = 0
				last_timestamp = time.time()
				while (time.time() - last_timestamp) < 0.3:
					current_dial_state = self.dial.is_pressed
					if not current_dial_state and last_dial_state == True:
						dialled_number = dialled_number + 1
						last_timestamp = time.time()
						
					last_dial_state = current_dial_state
					
				subprocess.call(['/usr/bin/espeak', '"Calling ' + str(dialled_number) + '"'])
				self.play()

				
		else:
			# Decide whether or not to ring
			if self.is_ringing or random.random() > 0.9:
				self.is_ringing = True
				
				# Ring until someone answers
				self.single_ring()

	def single_ring(self):
	# """"Short ringing burst""""
		for rings in range(0, 10):
			self.rightRing.off()
			self.leftRing.on()

			time.sleep(0.05)

			self.leftRing.off()
			self.rightRing.on()

			time.sleep(0.05)

			self.rightRing.off()
			self.leftRing.off()

	def record(self, seconds=5, filename='phone-recording.wav'):
	# """" Record a wave file from the handset""""
		rec_format = pyaudio.paInt16
		rec_channels = 1
		sample_rate=44100
		chunk = 512
		
		audio = pyaudio.PyAudio()

		stream = audio.open(format=rec_format,
						channels=rec_channels,
						rate=sample_rate,
						input=True,
						frames_per_buffer=chunk) #buffer

		frames = []

		for i in range(0, int(sample_rate / chunk * seconds)):
			data = stream.read(chunk)
			frames.append(data) # 2 bytes(16 bits) per channel

		stream.stop_stream()
		stream.close()
		audio.terminate()

		wf = wave.open(filename, 'wb')
		wf.setnchannels(rec_channels)
		wf.setsampwidth(audio.get_sample_size(rec_format))
		wf.setframerate(sample_rate)
		wf.writeframes(b''.join(frames))
		wf.close()
	
	def play(self, filename='phone-recording.wav'):
	# """" Play back a wave file through the handset""""
	
		chunk = 1024

		try:
			wf = wave.open(filename, 'rb')

			audio = pyaudio.PyAudio()

			stream = audio.open(format=audio.get_format_from_width(wf.getsampwidth()),
								channels=wf.getnchannels(),
								rate=wf.getframerate(),
								output=True)

			data = wf.readframes(chunk)

			while len(data) > 0:
				stream.write(data)
				data = wf.readframes(chunk)

			stream.stop_stream()
			stream.close()

			audio.terminate()
		except FileNotFoundError:
			print('WAV file not found')
	
	
	def kill(self):
	# """"Ensure the ringer is not powered when we exit""""
		self.rightRing.off()
		self.leftRing.off()

if __name__ == "__main__":
	try:
		phone = TardisPhone()
		while True:
			phone.loop()
			time.sleep(1)

	except KeyboardInterrupt:
		phone.kill()
		sys.exit(0)


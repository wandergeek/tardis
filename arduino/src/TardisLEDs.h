#define FASTLED_INTERRUPT_RETRY_COUNT 1
// #define FASTLED_ALLOW_INTERRUPTS 0
#include "TardisConstants.h"
#include "FastLED.h"

typedef struct {
  int startLED;
  int stopLED;
}  Pane;

typedef struct {
  CRGBPalette16 palette;
  int animationSpeed;
}  LedMode;

class TardisLEDs {
public:
  
  int currentAnimationSpeed;
  CRGBPalette16 currentPalette;
  TBlendType    currentBlending;
  CRGB leds[NUM_LEDS];
  unsigned long previousMillis;
  Pane allPanes[NUM_TOTAL_PANES];
  Pane signPanes[NUM_SIGN_PANES];
  Pane doorPanes[NUM_DOOR_PANES];
  Pane lamp;
  LedMode ledModes[NUM_LED_MODES];

/////////////////////// Setup ///////////////////////
  void setup() {
    randomSeed(analogRead(1));
    FastLED.addLeds<WS2812B, LED_DATA_PIN_1, GRB>(leds, STRIP_GROUP_1_START, STRIP_GROUP_2_START);
    FastLED.addLeds<WS2812B, LED_DATA_PIN_2, GRB>(leds, STRIP_GROUP_2_START, NUM_LEDS - STRIP_GROUP_2_START);
    FastLED.setBrightness(MAX_BRIGHTNESS);

    setupPanes();
    printPaneMapping();

    setupLedModes();

    currentPalette = black_p;
    // currentPalette = OceanColors_p;
    currentBlending = LINEARBLEND;
    currentAnimationSpeed = 10;
    // FillLEDsFromPaletteColors(0);
  };

//------------------------------------------------
  void setupPanes() {
    signPanes[0] = (Pane) { 0, 67 };
    signPanes[1] = (Pane) { 68, 136 };
    signPanes[2] = (Pane) { 137, 205 };
    signPanes[3] = (Pane) { 206, 275 };
    
    lamp = (Pane) {277,339};

    doorPanes[0] = (Pane) { 276, 352 };
    doorPanes[1] = (Pane) { 353, 391 };
    doorPanes[2] = (Pane) { 392, 430 };
    doorPanes[3] = (Pane) { 431, 469 };
    doorPanes[4] = (Pane) { 470, 508 };
    doorPanes[5] = (Pane) { 509, 547 };
    doorPanes[6] = (Pane) { 548, 587 };
    doorPanes[7] = (Pane) { 588, 627 };

    for(int i=0; i<NUM_SIGN_PANES; i++) {
      allPanes[i] = signPanes[i];
    }

    for(int i=0; i<NUM_DOOR_PANES; i++) {
      allPanes[NUM_SIGN_PANES + i] = doorPanes[i];
    }
  };

void setupLedModes() {
  ledModes[MODE_LURK] = {SetupChaserPalette(CRGB::White,6), 1};
  ledModes[MODE_BECKON] = {SetupChaserPalette(CRGB::White), 3};
  ledModes[MODE_DEMAT] = {blackwhite_p, 5};
  ledModes[MODE_MAT] = {blackwhite_p, 5};
  ledModes[MODE_LIVE] = {BlacK_Red_Magenta_Yellow_gp, 3}; 
  ledModes[MODE_666] = {heatmap_p, 5};
  ledModes[MODE_GAY] = {Rainbow_gp, 5};
  ledModes[MODE_420] = {fourtwenty_p, 5};
}
 
//------------------------------------------------
  void update(int daymode) {
    static uint8_t startIndex = 0;
    startIndex = startIndex + currentAnimationSpeed; /* motion speed */  
    if (!daymode) {
        FillLEDsFromPaletteColors(startIndex);
    } else {
        setColor(CRGB::Black);
    }
    FastLED.delay(1000 / UPDATES_PER_SECOND);
    FastLED.show();
  };


void setMode(LedMode m) {
  currentPalette = m.palette;
  currentAnimationSpeed = m.animationSpeed;
}

void setMode(int i) {
  LedMode m = ledModes[i];
  setMode(m);
}


/////////////////////// LED controllers ///////////////////////
  void setColor(CRGB c) {
    fill_solid( leds, NUM_LEDS, c);
    FastLED.show();
  };

//------------------------------------------------
  void lightPaneByID(CRGB c, int id) {
    Pane p = allPanes[id];
    Serial.print("lightPaneByID:"); Serial.print(p.startLED); Serial.print(":"); Serial.println(p.stopLED);
    setPaneColor(c, allPanes[id]);
    FastLED.show();
  };

//------------------------------------------------
  void lightLamp(CRGB c) {
    setPaneColor(c, lamp);
    FastLED.show();
  };

//------------------------------------------------
  void lightSignSolid(CRGB c) {
    for(int i=0; i<NUM_SIGN_PANES; i++) {
      setPaneColor(c, signPanes[i]);
    }
  };

//------------------------------------------------
  void flash(CRGB c, int flashTime) { //TODO: make this non-blocking
    fill_solid( leds, NUM_LEDS, c);
    FastLED.show();
    delay(flashTime);
  };

//------------------------------------------------
  void clearPane(Pane p) {
    setPaneColor(CRGB::Black, p);
  };

//------------------------------------------------
  void setPaneColor(CRGB c, Pane p) {
    for(int i=p.startLED; i<p.stopLED; i++) {
      leds[i] = c;
    }
  };

//------------------------------------------------
  void clearAllLEDS() {
    currentPalette = black_p;
  };

//------------------------------------------------
  void animateLEDS() {
    static uint8_t startIndex = 0;
    startIndex = startIndex + 1; /* motion speed */

    FillLEDsFromPaletteColors( startIndex);

    FastLED.show();
    FastLED.delay(1000 / UPDATES_PER_SECOND);
  };

//------------------------------------------------
  void FillLEDsFromPaletteColors( uint8_t colorIndex, Pane p) {
      uint8_t brightness = 255;

      for( int i = p.startLED; i < p.stopLED; i++) {
        leds[i] = ColorFromPalette( currentPalette, colorIndex, brightness, currentBlending);
        colorIndex += 3;
      }
    };

//------------------------------------------------
   void FillLEDsFromPaletteColors( uint8_t colorIndex, Pane p[]) {
      uint8_t brightness = 255;

      int numPanes = sizeof(p) / sizeof(p[0]);
      // Serial.print("FillLEDsFromPaletteColors: Passed in array of len "); Serial.println(numPanes);

      for (int i = 0; i<numPanes; i++) {
        for( int j = p[i].startLED; j < p[i].stopLED; j++) {
          leds[j] = ColorFromPalette( currentPalette, colorIndex, brightness, currentBlending);
          colorIndex += 3;
        }
      }
    };

//------------------------------------------------
  void FillLEDsFromPaletteColors( uint8_t colorIndex) {
    uint8_t brightness = 255;

    for( int i = 0; i < NUM_LEDS; i++) {
      leds[i] = ColorFromPalette( currentPalette, colorIndex, brightness, currentBlending);
      colorIndex += 3;
    }
  };





/////////////////////// Palette Generators ///////////////////////
  CRGBPalette16 SetupTotallyRandomPalette() {
    CRGBPalette16 palette;
  for( int i = 0; i < 16; i++) {
    palette[i] = CHSV( random8(), 255, random8());
  }

  return palette;
};

//------------------------------------------------
  CRGBPalette16 SetupChaserPalette(CRGB c) {
      CRGBPalette16 palette;
      // 'black out' all 16 palette entries...
      fill_solid( palette, 16, CRGB::Black);
      // and set every fourth one to white.
      palette[0] = c;
      palette[4] = c;
      palette[8] = c;
      palette[12] = c;

      return palette;
  };

  CRGBPalette16 SetupChaserPalette(CRGB c, int numpx) {
      CRGBPalette16 palette;
      // 'black out' all 16 palette entries...
      fill_solid( palette, 16, CRGB::Black);
      // and set every fourth one to white.
      for(int i=0; i<16; i+=numpx) {
        palette[i] = c;
      }

      return palette;
  };

/////////////////////// Test functions ///////////////////////
  void test() {
    setColor(CRGB::Red);
    delay(500);
    setColor(CRGB::Green);
    delay(500);
    setColor(CRGB::Blue);
    delay(500);
    setColor(CRGB::Black);
    clearAllLEDS();
  };

//------------------------------------------------
 void testPanes(){
    for(int i=0; i<NUM_SIGN_PANES; i++) {
      clearAllLEDS();
      setPaneColor(CRGB(255,0,255), allPanes[i]);
      // Serial.print("showing pane "); Serial.println(i);
      FastLED.show();
      delay(10000);
    }
  };

//------------------------------------------------
  void lightEachLED() {
    clearAllLEDS();
      for(int i=0; i<NUM_LEDS; i++) {
        leds[i] = CRGB(255,0,255);
        FastLED.show();
        // Serial.println(i);
        // delay(10);
      }
  };

//------------------------------------------------
  void lightEachLED(int start, int end) {
  clearAllLEDS();
  for(int i=start; i<end; i++) {
    leds[i] = CRGB(255,0,255);
    FastLED.show();
    // Serial.println(i);
    // delay(2000);
  }
};

//------------------------------------------------
 void printPaneMapping() {
    for(int i=0; i<NUM_TOTAL_PANES; i++) {
      // Serial.print("     "); Serial.print(allPanes[i].startLED); Serial.print(":"); Serial.println(allPanes[i].stopLED);
    }
  }


};

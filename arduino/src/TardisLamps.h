// #define FASTLED_ALLOW_INTERRUPTS 0
#include "TardisConstants.h"

//TODO use fewer functions -- beacon and floods are basically the same thing

class TardisLamps {

public:
    //TODO: if running out of memory, make these bytes
    int floodDirection = 1; //set to either 1 or -1
    int floodBrightness = 0; //0-255
    boolean b_floodFading = false; //is the flood currently fading?
    int floodFadeStep = 5; //speed in which fading happens

    int beaconDirection = 1;
    int beaconBrightness = 0;
    boolean b_beaconFading = false;
    int beaconFadeStep = 5; 


    void setup() {
        //beacon setup
        analogWrite(BEACON_PIN, 0);
        pinMode(BEACON_PIN, OUTPUT);
        pinMode(BEACON_ENABLE_PIN, OUTPUT);
        digitalWrite(BEACON_ENABLE_PIN, HIGH);

        //flood setup
        analogWrite(FLOOD_PIN, 0);
        pinMode(FLOOD_PIN, OUTPUT);
        pinMode(FLOOD_ENABLE_PIN, OUTPUT);
        digitalWrite(FLOOD_ENABLE_PIN, HIGH);
    };

    void update(int daymode) {
        if (!daymode) {
        if(b_floodFading) {
            floodBrightness += (floodDirection*floodFadeStep); //adds/subtracts floodfadestep
            if(floodBrightness >= 255) {
                floodDirection = -1;
                floodBrightness = 255;
            } else if(floodBrightness <= 0) {
                floodDirection = 1;
                floodBrightness = 0;
            }
            setFloods(floodBrightness);
        }
        

        if(b_beaconFading) {
            beaconBrightness = beaconBrightness + (beaconDirection*beaconFadeStep); 
            if(beaconBrightness >= 255) {
                beaconDirection = -1;
                beaconBrightness = 255;
            } else if(beaconBrightness <= 0) {
                beaconDirection = 1;
                beaconBrightness = 0;
            }
            setBeacon(beaconBrightness);
        }
        }  // can't figure out how to indent        
    };

    void setFloods(int val) {
        floodBrightness = constrain(val, 0, 255); 
        // Serial.print("Setting floods: ");Serial.println(floodBrightness);
        analogWrite(FLOOD_PIN,floodBrightness);
    };

    void setFloodFadeSpeed(int val) {
        floodFadeStep = val;
    };

    void setBeaconFadeSpeed(int val) {
        beaconFadeStep = val;
    };

    void setLampSpeed(int val) {
        setBeaconFadeSpeed(val);
        setFloodFadeSpeed(val);
    }

    void setBeacon(int val) {
        beaconBrightness = constrain(val, 0, 255); 
        // Serial.print("Setting beacon: ");Serial.println(beaconBrightness);
        analogWrite(BEACON_PIN,beaconBrightness);
    };

    void turnOffAll() {
        disableBeaconFading();
        disableFloodFading();
        turnOffFloods();
        turnOffBeacon();
    };

    void turnOnAll() {
        turnOnFloods();
        turnOnBeacon();
    };

    void turnOnFloods() {
        setFloods(255);
    };

    void turnOnBeacon() {
        setBeacon(255);
    };

    void turnOffFloods() {
        setFloods(0);
    };

    void turnOffBeacon() {
        setBeacon(0);
    };

    void enableBeaconFading() {
        b_beaconFading = true;
    };

    void enableFloodFading() {
        b_floodFading = true;
    };

    void disableBeaconFading() {
        b_beaconFading = false;
    };

    void disableFloodFading() {
        b_floodFading = false;
    };

    void test() {
        for(int i=0; i<5; i++) {
            turnOnAll();
            delay(100);
            turnOffAll();
            delay(100);
        }

    };   

};

    

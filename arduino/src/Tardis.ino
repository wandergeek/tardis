#include "FastLED.h"
#include <SimpleModbusSlave.h>

#include "TardisLEDs.h"
#include "TardisLamps.h"
#include "TardisConstants.h"

#define DAY_MODE (0)

TardisLEDs leds;
TardisLamps lamps;
int curMode = 0;
CRGB curColor;
boolean b_motionDetected;

//serial shit
unsigned int holdingRegs[TOTAL_REGS_SIZE];

/////////////////////// Setup ///////////////////////
void setup() {
  //serial
  // Serial.begin(BAUD_RATE);
  // Serial.println("Waking up...");

  //leds
  leds.setup();
  leds.test();

  //lamps
  lamps.setup();
  lamps.test();

  //motion
  pinMode(MOTION_PIN_1, INPUT);
  attachInterrupt(0, motionDetected, CHANGE);
  b_motionDetected = false; //interrupt toggles this boolean

  //modbus
  modbus_configure(BAUD_RATE, 1, 0, TOTAL_REGS_SIZE, 0);

  Serial1.begin(9600);
  Serial1.println("Init sequence complete");

}

/////////////////////// Main Loop ///////////////////////

void loop() {
  // leds.testPanes();
  // lamps.enableFloodFading();
  // leds.testPanes();
  // leds.lightPaneByID(CRGB::White, 0);
  // leds.lightSignSolid(CRGB::White);
  // leds.lightEachLED();
  // leds.lightEachLED(552,570);
  // leds.testPanes();
    //  leds.lightLamp(CRGB::White);
    //  delay(1000);
    //  leds.clearAllLEDS();
  // lamps.turnOnAll();
  // leds.test();
  // lamps.test();

  holdingRegs[TOTAL_ERRORS_REG] = modbus_update(holdingRegs);
  
  if(curMode != holdingRegs[MODE_REG]) {
    printRegs();
    Serial1.print("Setting mode: "); Serial1.println(holdingRegs[MODE_REG]);
    setMode(holdingRegs[MODE_REG]);
    
  }
  
  // leds.update(DAY_MODE);
  // lamps.update(DAY_MODE);
}


void printRegs() {
  for (int i=0; i<TOTAL_REGS_SIZE; i++) {
    Serial1.print(holdingRegs[i]);
  }
  Serial1.println("");
}
//TODO: first against the wall in refactor
void setMode(int mode) {

  curMode = mode;
  
  switch(mode) {
    case MODE_LURK:
      leds.setMode(MODE_LURK);
      break;

    case MODE_BECKON:
      leds.setMode(MODE_BECKON);
      break;

    case MODE_MAT:
      leds.setMode(MODE_MAT);
      break;

    case MODE_DEMAT:
      leds.setMode(MODE_DEMAT);
      break;

    case MODE_LIVE:
      leds.setMode(MODE_LIVE);
      break;

    case MODE_666:
      leds.setMode(MODE_666);
      break;

    case MODE_420:
      leds.setMode(MODE_420);
      break;

    case MODE_GAY:
      leds.setMode(MODE_GAY);
      break;

    case FAST_FADE_FLOODS:
      lamps.setLampSpeed(FAST_FADE_SPEED);
      lamps.setBeacon(255); //so beacon and floods are out of phase
      lamps.enableFloodFading();
      lamps.enableBeaconFading();
      break;

    case SLOW_FADE_FLOODS:
      lamps.setLampSpeed(SLOW_FADE_SPEED);
      lamps.setBeacon(255); //so beacon and floods are out of phase
      lamps.enableFloodFading();
      lamps.enableBeaconFading();
      break;

    
    case KILL_LAMPS:
      lamps.turnOffAll();
      break;

    case MODE_INIT:
      lamps.turnOffAll();
      leds.clearAllLEDS();
      break;

  }
}

//room for growth here, can use strtok to get tokenized commands
//packet format: <data,data,data>
// void parseData() {
  // int mode = atoi(inputBuffer);
  // Serial.print("Got mode:"); Serial.println(mode);
  // leds.lightEachLED(0,mode);
  // setMode(mode);     
// }

//------------------------------------------------
void motionDetected() {
  Serial.println("Motion detected");
  int sensorVal = digitalRead(MOTION_PIN_1);
  if(sensorVal == 1) {
    //enable on rising edge
    b_motionDetected = true;
    holdingRegs[MOTION_DETECT_REG] = 1;
    Serial1.println("Motion detected");
    printRegs();
  } else {
    //reset on falling edge
    holdingRegs[MOTION_DETECT_REG] = 0;
    b_motionDetected = false;
  }
}


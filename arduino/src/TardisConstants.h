#ifndef TARDIS_INCLUDE
#define TARDIS_INCLUDE

#define BAUD_RATE 9600


//Pins
#define LED_DATA_PIN_1 15
#define LED_DATA_PIN_2 14
#define MOTION_PIN_1 2
#define BEACON_PIN 3
#define BEACON_ENABLE_PIN 21
#define FLOOD_PIN 5
#define FLOOD_ENABLE_PIN 20

//LEDs
#define STRIP_GROUP_1_START 0
#define STRIP_GROUP_2_START 337
#define NUM_LEDS 650
#define MAX_BRIGHTNESS 100
#define UPDATES_PER_SECOND 75
#define NUM_DOOR_PANES 8
#define NUM_SIGN_PANES 4
#define NUM_TOTAL_PANES 12
#define NUM_LEDS_PER_WINDOW 39
#define NUM_LEDS_PER_SIGN 68
#define NUM_LED_MODES 10


//Serial
const byte TOTAL_REGS_SIZE = 50;
byte TOTAL_ERRORS_REG = TOTAL_REGS_SIZE - 3;
byte MOTION_DETECT_REG = TOTAL_REGS_SIZE - 2;
byte MODE_REG = 0;

//Modes
#define MODE_LURK 0
#define MODE_BECKON 1
#define MODE_MAT 2
#define MODE_LIVE 3
#define MODE_DEMAT 4
#define MODE_666 5
#define MODE_GAY 6
#define MODE_420 7
#define MODE_INIT 8

//Messages
#define FAST_FADE_FLOODS 9
#define SLOW_FADE_FLOODS 21
#define KILL_LAMPS 22

//lamps
#define FAST_FADE_SPEED 20
#define SLOW_FADE_SPEED 40


//Messages
#define MOTION_DETECTED 10

DEFINE_GRADIENT_PALETTE( blackwhite_p ) {
  0,     0,  0,  0,   //black
255,   255,255,255 }; //full white

//this is probably completley unnecessary
DEFINE_GRADIENT_PALETTE( black_p ) {
  0,     0,  0,  0,   
255,     0,  0,  0 }; 

DEFINE_GRADIENT_PALETTE( heatmap_p ) {
  0,     0,  0,  0,   //black
128,   255,  0,  0,   //red
224,   255,255,  0,   //bright yellow
255,   255,255,255 }; //full white

DEFINE_GRADIENT_PALETTE( fourtwenty_p ) {
  0,     186,  22,  33, //red 
85,     251,  201,  38,  //yellow
255,     29,    100,   48 }; //red

DEFINE_GRADIENT_PALETTE( es_ocean_breeze_068_gp ) {
    0, 100,156,153,
   51,   1, 99,137,
  101,   1, 68, 84,
  104,  35,142,168,
  178,   0, 63,117,
  255,   1, 10, 10};




DEFINE_GRADIENT_PALETTE( ocean_p ) {
  0, 25,25,112,
  16,0,0,139,
  32,0, 0, 128,
  48,0,0,139,
  64,0, 0, 205,
  80,46, 139, 87,
  96,0, 128, 128,
  112,95, 158, 160,
  128,0, 0, 255,
  144,0, 139, 139,
  160,100, 149, 237,
  176,127, 255, 212,
  192,46, 139, 87,
  208,0, 255, 255,
  224, 135, 206, 250 };


DEFINE_GRADIENT_PALETTE( BlacK_Magenta_Red_gp ) {
    0,   0,  0,  0,
   63,  42,  0, 45,
  127, 255,  0,255,
  191, 255,  0, 45,
  255, 255,  0,  0};

DEFINE_GRADIENT_PALETTE( BlacK_Red_Magenta_Yellow_gp ) {
    0,   0,  0,  0,
   42,  42,  0,  0,
   84, 255,  0,  0,
  127, 255,  0, 45,
  170, 255,  0,255,
  212, 255, 55, 45,
  255, 255,255,  0};



#endif

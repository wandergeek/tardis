#include <avr/power.h>
//#include "SimpleModbusSlave.h"
//#define FASTLED_ALLOW_INTERRUPTS 0
#include <FastLED.h>

#define STRIP_LED_GROUP_1_DATA_PIN (14)
#define STRIP_LED_GROUP_2_DATA_PIN (15)

#define PIR_PIN (7)

#define NUM_STRIP_LEDS (595)
#define STRIP_GROUP_1_START (0)
#define STRIP_GROUP_2_START (281)

CRGB stripLeds[NUM_STRIP_LEDS];

uint16_t dotPosition = 0;
uint16_t ledIndex;
uint8_t ledHue = 100;
uint8_t ledSaturation = 180;
uint8_t ledBrightness = 0;
//uint8_t red;
//uint8_t green;
//uint8_t blue;
int fadeDirection = 1;
int floodDirection = 1;
int floodBrightness = 1;
#define MAX_STRIP_BRIGHTNESS (100)

#define LED_BANK_A_ENABLE_PIN (21)  
#define LED_BANK_A_RIGHT_PWM_PIN (3)
#define LED_BANK_A_LEFT_PWM_PIN (2)
#define LED_BANK_B_ENABLE_PIN (20)  
#define LED_BANK_B_RIGHT_PWM_PIN (5)
#define LED_BANK_B_LEFT_PWM_PIN (4)
#define ONBOARD_LED_PIN (13)
#define TX_ENABLE_PIN (8)

#define SLAVE_ID (1)

#define BAUD_RATE (9600)
#define BYTE_FORMAT SERIAL_8N1

typedef enum {
  BANK_A_LEFT_PWM_REG = 0,
  BANK_A_RIGHT_PWM_REG,
  BANK_B_LEFT_PWM_REG,
  BANK_B_RIGHT_PWM_REG,
  NUM_HOLDING_REGS,
} holdingRegs_t;

unsigned int holdingRegs[NUM_HOLDING_REGS];

void setup() {

  Serial.begin(9600);
  
  if (F_CPU == 16000000) {
    clock_prescale_set(clock_div_1);
  }

  memset(holdingRegs, 0, sizeof(holdingRegs));
            
  // modbus_configure(&Serial, BAUD_RATE, BYTE_FORMAT, SLAVE_ID, TX_ENABLE_PIN,  NUM_HOLDING_REGS, holdingRegs);
  
  analogWrite(LED_BANK_A_LEFT_PWM_PIN, 0);
  pinMode(LED_BANK_A_LEFT_PWM_PIN, OUTPUT);
  
  analogWrite(LED_BANK_A_RIGHT_PWM_PIN, 0);
  pinMode(LED_BANK_A_RIGHT_PWM_PIN, OUTPUT);
  
  analogWrite(LED_BANK_B_LEFT_PWM_PIN, 0);
  pinMode(LED_BANK_B_LEFT_PWM_PIN, OUTPUT);
  
  analogWrite(LED_BANK_B_RIGHT_PWM_PIN, 0);
  pinMode(LED_BANK_B_RIGHT_PWM_PIN, OUTPUT);
  
  digitalWrite(LED_BANK_A_ENABLE_PIN, LOW);
  pinMode(LED_BANK_A_ENABLE_PIN, OUTPUT); 
 
  digitalWrite(LED_BANK_B_ENABLE_PIN, LOW);
  pinMode(LED_BANK_B_ENABLE_PIN, OUTPUT); 
  
  digitalWrite(ONBOARD_LED_PIN, LOW);
  pinMode(ONBOARD_LED_PIN, OUTPUT); 

  pinMode(PIR_PIN, INPUT);
  
  
  FastLED.addLeds<WS2812B, STRIP_LED_GROUP_1_DATA_PIN, GRB>(stripLeds, STRIP_GROUP_1_START, STRIP_GROUP_2_START);
  FastLED.addLeds<WS2812B, STRIP_LED_GROUP_2_DATA_PIN, GRB>(stripLeds, STRIP_GROUP_2_START, NUM_STRIP_LEDS - STRIP_GROUP_2_START);
//  FastLED.addLeds<WS2812B, STRIP_LED_GROUP_2_DATA_PIN, GRB>(stripLeds, 0, NUM_STRIP_LEDS - STRIP_GROUP_2_START);
  
  for (ledIndex = 0; ledIndex < NUM_STRIP_LEDS; ledIndex++) {
    stripLeds[ledIndex] = CRGB::Black;
  }
  FastLED.show();
  
}


void setAllFloods(int val) {
  analogWrite(LED_BANK_A_LEFT_PWM_PIN,val);
  analogWrite(LED_BANK_A_RIGHT_PWM_PIN,val);
  analogWrite(LED_BANK_B_LEFT_PWM_PIN,val);
  analogWrite(LED_BANK_B_RIGHT_PWM_PIN,val);
}

void loop() {
  
//  Serial.println(digitalRead(PIR_PIN));
  
  // modbus_update();
 
//  digitalWrite(LED_BANK_A_ENABLE_PIN, (holdingRegs[BANK_A_LEFT_PWM_REG] + holdingRegs[BANK_A_RIGHT_PWM_REG]) > 0);
//  digitalWrite(LED_BANK_B_ENABLE_PIN, (holdingRegs[BANK_B_LEFT_PWM_REG] + holdingRegs[BANK_B_RIGHT_PWM_REG]) > 0);
  
  
//  analogWrite(LED_BANK_A_LEFT_PWM_PIN, holdingRegs[BANK_A_LEFT_PWM_REG]);
//  analogWrite(LED_BANK_A_RIGHT_PWM_PIN, holdingRegs[BANK_A_RIGHT_PWM_REG]);
//  analogWrite(LED_BANK_B_LEFT_PWM_PIN, holdingRegs[BANK_B_LEFT_PWM_REG]);
//  analogWrite(LED_BANK_B_RIGHT_PWM_PIN, holdingRegs[BANK_B_RIGHT_PWM_REG]);

  digitalWrite(LED_BANK_A_ENABLE_PIN, 1);
  digitalWrite(LED_BANK_B_ENABLE_PIN, 1); 
  
  analogWrite(LED_BANK_A_LEFT_PWM_PIN,255);
  analogWrite(LED_BANK_A_RIGHT_PWM_PIN,255);
  analogWrite(LED_BANK_B_LEFT_PWM_PIN,255);
  analogWrite(LED_BANK_B_RIGHT_PWM_PIN,255);

  digitalWrite(ONBOARD_LED_PIN, dotPosition % 2);
  
 
  delay(30);  // ms
  

 
  // H-BRIDGE
//
//  if (holdingRegs[BANK_A_LEFT_PWM_REG]) {
//    memset(holdingRegs, 0, sizeof(holdingRegs));
//  } else {
//    memset(holdingRegs, 100, sizeof(holdingRegs));
//  }
// 
  // STRIP
//  
//  if (fadeDirection) {
//    ledBrightness++;
//  } else {
//    ledBrightness--;
//  }
  
//  if (ledBrightness >= MAX_STRIP_BRIGHTNESS) {
//    fadeDirection = 0;
//  } else if (ledBrightness == 0) {
//    fadeDirection = 1;
//    ledHue = random(255);
//  } 
//
//  if (++dotPosition >= NUM_STRIP_LEDS) {
//    dotPosition = 0;
//  }

//   if (floodBrightness >= 255) {
//      floodDirection = -1;
//   } else if (floodBrightness == 0) {
//      floodDirection = 1;
//   }
//
//   floodBrightness += floodDirection;
//    Serial.println(floodBrightness);
//   setAllFloods(254);
  
//  stripLeds[(dotPosition + 0) % NUM_STRIP_LEDS] = CHSV(50, 150, 255);
//  stripLeds[(dotPosition + 10) % NUM_STRIP_LEDS] = CHSV(150, 150, 255);
//  stripLeds[(dotPosition + 20) % NUM_STRIP_LEDS] = CHSV(250, 150, 255);
//  stripLeds[(dotPosition + 100) % NUM_STRIP_LEDS] = CHSV(50, 150, 255);
//  stripLeds[(dotPosition + 110) % NUM_STRIP_LEDS] = CHSV(150, 150, 255);
//  stripLeds[(dotPosition + 120) % NUM_STRIP_LEDS] = CHSV(250, 150, 255);
//  stripLeds[(dotPosition + 200) % NUM_STRIP_LEDS] = CHSV(50, 150, 255);
//  stripLeds[(dotPosition + 210) % NUM_STRIP_LEDS] = CHSV(150, 150, 255);
//  stripLeds[(dotPosition + 220) % NUM_STRIP_LEDS] = CHSV(250, 150, 255);  
//  stripLeds[(dotPosition + 300) % NUM_STRIP_LEDS] = CHSV(50, 150, 255);
//  stripLeds[(dotPosition + 310) % NUM_STRIP_LEDS] = CHSV(150, 150, 255);
//  stripLeds[(dotPosition + 320) % NUM_STRIP_LEDS] = CHSV(250, 150, 255); 
//  stripLeds[(dotPosition + 400) % NUM_STRIP_LEDS] = CHSV(50, 150, 255);
//  stripLeds[(dotPosition + 410) % NUM_STRIP_LEDS] = CHSV(150, 150, 255);
//  stripLeds[(dotPosition + 420) % NUM_STRIP_LEDS] = CHSV(250, 150, 255); 
//  stripLeds[(dotPosition + 500) % NUM_STRIP_LEDS] = CHSV(50, 150, 255);
//  stripLeds[(dotPosition + 510) % NUM_STRIP_LEDS] = CHSV(150, 150, 255);
//  stripLeds[(dotPosition + 520) % NUM_STRIP_LEDS] = CHSV(250, 150, 255); 
//  
//  FastLED.show();
  
//
//  for(int whiteLed = 0; whiteLed < NUM_STRIP_LEDS; whiteLed = whiteLed + 1) {
//      // Turn our current led on to white, then show the leds
//      stripLeds[whiteLed] = CRGB::White;
//      set
//      FastLED.show();
//      stripLeds[whiteLed] = CRGB::Black;
//  }

  
}

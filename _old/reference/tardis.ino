#include "FastLED.h"
#define NUM_LEDS 4 
CRGB leds[NUM_LEDS];
#define BRIGHTNESS  64
#define DATA_PIN 2

int fadeAmount = 5;  
int brightness = 0; 

int mode = 0;

enum serial_msg {
  MOTION_DETECTED = 10,
  INIT = 11
};


void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  FastLED.setBrightness(  BRIGHTNESS );
  fill_solid( leds, NUM_LEDS, CRGB::Black);
  FastLED.show();
}

void loop() {
 if (Serial.available()) {
    int read_int = Serial.parseInt();
    if(read_int != 0) {
      mode = read_int;
      Serial.print("got mode: "); Serial.println(mode, DEC);
    }
  }
  
  digitalWrite(LED_BUILTIN, HIGH);
  delay(500);
  enum serial_msg msg = MOTION_DETECTED;
  Serial.println(msg);
  digitalWrite(LED_BUILTIN, LOW);
  delay(30000);

  switch(mode) {
    case 1:
      fadeMode();
      break;
    case 9:
      resetMode();
      break; 
  }
}


void fadeMode() {
   for(int i = 0; i < NUM_LEDS; i++ )
   {
   leds[i].setRGB(255,255,250); 
   leds[i].fadeLightBy(brightness);
  }
  FastLED.show();
  brightness = brightness + fadeAmount;
  if(brightness == 0 || brightness == 255)
  {
    fadeAmount = -fadeAmount ; 
  }    
  delay(9);
}

void resetMode() {
  fill_solid( leds, NUM_LEDS, CRGB::Black);
  FastLED.show();
}



#include "FastLED.h"

class Pane{

    private:
        CRGB leds;
        int num_leds;
    
    public:
        void setup(CRGB *_leds, int _num_leds) {
            leds = &_leds;
            num_leds = _num_leds;
        }

};

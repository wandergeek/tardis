#include "FastLED.h"
#include "Pane.h"

CRGB all_leds[100];

void setup() {
    FastLED.addLeds<NEOPIXEL, 1>(all_leds, 100);
    Pane p;
    p.setup(&all_leds[100],10);
}

void loop() {

}


import time
import subprocess
import threading 
from flask import Flask
app = Flask(__name__)

@app.route("/reboot")
def servereboot():
    th = threading.Thread(target=reboot)
    th.start()
    return "Rebooting tardis..."

@app.route("/halt")
def servehalt():
    th = threading.Thread(target=halt)
    th.start()
    return "Shutdown sequence initiated..."

def reboot():
    time.sleep(3)
    subprocess.call(["reboot"])

def halt():
    time.sleep(3)
    subprocess.call(["halt"])

if __name__ == '__main__':
          app.run(host='0.0.0.0', port=8080)

# Google speech to text

From this directory: 

```
export GOOGLE_APPLICATION_CREDENTIALS="`pwd`/gcloud-auth.json"
cd python-docs-samples/speech/cloud-client 
python transcribe.py resources/test.wav
```

WAV files need to be created with a certain sample rate. Need to get streaming working next.

# TARDIS
Code and sketches for TARDIS - installation from Burning Seed 2018 

## Control
### Restart tardis
```
systemctl restart tardis-phone
```

### Stop tardis
```
systemctl stop tardis-phone
```

### Get logs
```
journalctl -u tardis-phone
```

### Follow logs
```
journalctl -f -u tardis-phone
```

### Send serial messsages
`screen /dev/ttyACM0 9600`
* <8> - turn everything off
  

## Arduino
### Building
```
sudo su
cd /home/pi/tardis/arduino
make && make upload
```

## Important bits

`arduino/src/TardisContents.h`
* lots of timings/max brightness
* message mappings
* Color palettes

`pi/Tardis/app.py`
* main entry point
* timings for lurk/beckon/materialize/live up top. Flow is lurk <-> beckon -> materialize -> live -> lurk
  
    * LURK_TIMEOUT = 300
    * BECKON_TIMEOUT = 60
    * MAT_TIMEOUT = 15
    * DEMAT_TIMEOUT = 10
    * LIVE_TIMEOUT = 300

`pi/Tardis/contacts`
* main interactions, edit (or add!) a .py in a similar format and it'll create a new contact




